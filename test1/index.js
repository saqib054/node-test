const assert = require('chai').assert;

const names = [
  'Michael Daniel Jäger',
  'LINUS HARALD christer WAHLGREN',
  'Pippilotta Viktualia Rullgardina Krusmynta Efraimsdotter LÅNGSTRUMP',
  'Kalle Anka',
  'Ghandi',
];

const expected = [
  { first: 'Michael', middle: ['Daniel'], last: 'Jäger' },
  { first: 'Linus', middle: ['Harald', 'Christer'], last: 'Wahlgren' },
  {
    first: 'Pippilotta',
    middle: ['Viktualia', 'Rullgardina', 'Krusmynta', 'Efraimsdotter'],
    last: 'Långstrump',
  },
  { first: 'Kalle', middle: [], last: 'Anka' },
  { first: 'Ghandi', middle: [], last: null },
];

const validate = (result) => {
  try {
    assert.deepEqual(result, expected);
  } catch (e) {
    console.error('Failed', e);
  }
};

// implement code generating result

const capitalize = (str) => {
  return str
    .trim()
    .toLowerCase()
    .replace(/\w\S*/g, (w) => w.replace(/^\w/, (c) => c.toUpperCase()));
};

const result = names.map((name) => {
  const nameObject = {};
  const names = name.split(' ');
  const firstName = names.shift();
  const lastName = names.pop();
  const middleName = names;
  nameObject.first = firstName ? capitalize(firstName) : null;
  nameObject.middle = middleName
    ? middleName.map((name) => capitalize(name))
    : null;
  nameObject.last = lastName ? capitalize(lastName) : null;
  return nameObject;
});

// At the end call validate
validate(result);
